package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Dado dados = new Dado();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Digite a qtde de lados do dado: ");
        int qtdeLados = scanner.nextInt();

        dados.setLadosDado(qtdeLados);

        dados.geraDado();
        dados.sortearDado();
    }
}
